from django.conf.urls import url
from catalog import views


urlpatterns = [
    url(r"^users/user-details/", views.private),
]
